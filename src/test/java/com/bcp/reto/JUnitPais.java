package com.bcp.reto;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bcp.reto.model.Pais;
import com.bcp.reto.model.TipoCambio;
import com.bcp.reto.repository.PaisRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
class PaisApplicationTests {
	@Autowired
	PaisRepository repository;
	
	@Test
    public void testRepository() {
		List<Pais> listPais= new ArrayList<Pais>();
		listPais=repository.findAll();
        assertThat( ((Pais)listPais.get(0)).getNombre().equals("Peru"));
	}

}

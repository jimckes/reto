package com.bcp.reto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bcp.reto.model.TipoCambio;
import com.bcp.reto.repository.TipoCambioRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
class TipoCambioRepositoryTest {
	@Autowired
	TipoCambioRepository repository;

	@Test
    public void testRepository() {
		TipoCambio tc=new TipoCambio();
        tc.setCodigo("USD");
        tc.setMoneda("Dólar estadounidense");
        tc.setTipoValor(3.729);
        repository.save(tc);
        assertThat(tc.getId()).isNotNull();
	}

}

package com.bcp.reto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bcp.reto.model.TipoCambio;
import com.bcp.reto.response.BaseWebResponse;
import com.bcp.reto.response.TipoCambioRequest;
import com.bcp.reto.response.TipoCambioResponse;
import com.bcp.reto.service.TipoCambioService;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

@RestController
@RequestMapping(value = "/api/cambio")
public class TipoCambioController {
	
    @Autowired
    private TipoCambioService tipoCambioService;
    
    @PostMapping
    public Single<ResponseEntity<BaseWebResponse<TipoCambioResponse>>> getConversion(@RequestBody TipoCambioRequest addAuthorWebRequest) {
       
    	return tipoCambioService.convertir(addAuthorWebRequest)
                .subscribeOn(Schedulers.io())
                .map(tipoCambioResponse -> ResponseEntity.ok(BaseWebResponse.successWithData(tipoCambioResponse)));
    	    	
    }   
    
  
    
    @PutMapping
    public Completable updateTipoCambio(@RequestBody TipoCambio tipoCambio ) {       
    	return tipoCambioService.actualizarTipoCambio(tipoCambio);
    			
    } 
    
    @GetMapping
    public ResponseEntity<Single<ResponseEntity<List<TipoCambio>>>> listarTC() {

    	return new ResponseEntity(tipoCambioService.listar(), HttpStatus.OK);
    	
    }
    
    @GetMapping("/listarPaises")
    public ResponseEntity<Single<ResponseEntity<List<TipoCambio>>>> listarPaises() {

    	return new ResponseEntity(tipoCambioService.listarPaises(), HttpStatus.OK);
    	
    }
}

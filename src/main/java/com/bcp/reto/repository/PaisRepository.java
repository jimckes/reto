package com.bcp.reto.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcp.reto.model.Pais;
import com.bcp.reto.model.TipoCambio;
@Repository
public interface PaisRepository extends JpaRepository<Pais,String>{
//	public Pais findByCodigo(String codigo); 
}

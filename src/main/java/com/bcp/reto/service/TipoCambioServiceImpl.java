package com.bcp.reto.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bcp.reto.model.Pais;
import com.bcp.reto.model.TipoCambio;
import com.bcp.reto.repository.PaisRepository;
import com.bcp.reto.repository.TipoCambioRepository;
import com.bcp.reto.response.TipoCambioRequest;
import com.bcp.reto.response.TipoCambioResponse;

import io.reactivex.Completable;
import io.reactivex.Single;

@Service
public class TipoCambioServiceImpl implements TipoCambioService{

	@Autowired
	private TipoCambioRepository tipoCambioRepository;
	
	@Autowired
	private PaisRepository paisRepository;
	
	@Override
	public Single<TipoCambioResponse> convertir(TipoCambioRequest tipoCambioRequest) {
		 return addCambioToRepository(tipoCambioRequest);
	}
    private Single<TipoCambioResponse> addCambioToRepository(TipoCambioRequest tipoCambioRequest) {
        return Single.create(singleSubscriber -> {
            
            TipoCambioResponse cambioResponse=new TipoCambioResponse(); 
            
            TipoCambio tcOrigen=tipoCambioRepository.findByCodigo(tipoCambioRequest.getMonedaOrigen());
            TipoCambio tcFinal=tipoCambioRepository.findByCodigo(tipoCambioRequest.getMonedaDestino());
            double montoSoles=0;
            double montoFinal=0;
            
            if(!tcOrigen.getCodigo().equals("PEN")) {
            	//convierto a soles
            	montoSoles=tipoCambioRequest.getMonto()*tcOrigen.getTipoValor();
            	//convierto a moneda final
            	montoFinal=montoSoles/tcFinal.getTipoValor();            	
            }else {
            	// esta en soles
            	montoFinal=tipoCambioRequest.getMonto()/tcFinal.getTipoValor();
            }
            BigDecimal bdMontoFinal = BigDecimal.valueOf(montoFinal);
            bdMontoFinal = bdMontoFinal.setScale(3, RoundingMode.HALF_UP);
            cambioResponse.setMonto(tipoCambioRequest.getMonto());  
            cambioResponse.setMontoTipoCambio(bdMontoFinal.doubleValue());
            cambioResponse.setMonedaOrigen(tcOrigen.getMoneda());
            cambioResponse.setMonedaDestino(tcFinal.getMoneda());
            BigDecimal bdTc = BigDecimal.valueOf(cambioResponse.getMontoTipoCambio()/tipoCambioRequest.getMonto());
            bdTc = bdTc.setScale(3, RoundingMode.HALF_UP);
            cambioResponse.setTipoCambio(bdTc.doubleValue() + tcFinal.getCodigo());            
            singleSubscriber.onSuccess(cambioResponse);
        });
    }
	@Override
	public void guardar(TipoCambio tipoCambio) {
		tipoCambioRepository.save(tipoCambio);		
	}
	
	
	
	
	@Override
	public Completable actualizarTipoCambio(TipoCambio updateTipoCambio) {
		return updateTipoCambioToRepository(updateTipoCambio);
	}
	private Completable updateTipoCambioToRepository(TipoCambio updateTipoCambio) {
        return Completable.create(completableSubscriber -> {
            TipoCambio tipoCambio = tipoCambioRepository.findByCodigo(updateTipoCambio.getCodigo());
            if (tipoCambio==null)
                completableSubscriber.onError(new EntityNotFoundException("No encontro el codigo de moneda"));
            else {
                tipoCambio.setTipoValor(updateTipoCambio.getTipoValor());
                tipoCambioRepository.save(tipoCambio);
                completableSubscriber.onComplete();
            }
        });
	}
	@Override
	public Single<List<TipoCambio>>  listar() {
        return Single.create(singleSubscriber -> {
            singleSubscriber.onSuccess(tipoCambioRepository.findAll());
        });
	}
	@Override
	public Single<List<Pais>> listarPaises() {
		return Single.create(singleSubscriber -> {
            singleSubscriber.onSuccess(paisRepository.findAll());
        });
	}
}

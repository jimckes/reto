package com.bcp.reto.service;


import java.util.List;

import com.bcp.reto.model.Pais;
import com.bcp.reto.model.TipoCambio;
import com.bcp.reto.response.TipoCambioRequest;
import com.bcp.reto.response.TipoCambioResponse;

import io.reactivex.Completable;
import io.reactivex.Single;


public interface TipoCambioService {
	Single<TipoCambioResponse> convertir(TipoCambioRequest tipoCambioRequest);
	public void guardar(TipoCambio tipoCambio);
	Completable actualizarTipoCambio(TipoCambio updateTipoCambio);//Completablerepresentan Observableque no emite ningún valor, 
	Single<List<TipoCambio>> listar();
	Single<List<Pais>> listarPaises();
}

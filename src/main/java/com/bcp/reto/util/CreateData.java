package com.bcp.reto.util;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.bcp.reto.model.TipoCambio;
import com.bcp.reto.security.model.Rol;
import com.bcp.reto.security.model.RolNombre;
import com.bcp.reto.security.model.Usuario;
import com.bcp.reto.security.service.RolService;
import com.bcp.reto.security.service.UsuarioService;
import com.bcp.reto.service.TipoCambioService;


/**
*crea los roles al ejecutar la aplicacion
 *
 */

@Component
public class CreateData implements CommandLineRunner {

    @Autowired
    RolService rolService;
    @Autowired
    TipoCambioService tipoCambioService;
    @Autowired
    UsuarioService usuarioService;

    @Override
    public void run(String... args) throws Exception {
        Rol rolAdmin = new Rol(RolNombre.ROLE_ADMIN);
        Rol rolUser = new Rol(RolNombre.ROLE_USER);
//        rolService.save(rolAdmin);
//        rolService.save(rolUser);
//        
//        
//        TipoCambio tc=new TipoCambio();
//        tc.setCodigo("USD");
//        tc.setMoneda("Dólar estadounidense");
//        tc.setTipoValor(3.729);
//        tipoCambioService.guardar(tc);
//        
//        tc=new TipoCambio();
//        tc.setCodigo("EUR");
//        tc.setMoneda("Euro");
//        tc.setTipoValor(4.353);
//        tipoCambioService.guardar(tc);
//        
//        tc=new TipoCambio();
//        tc.setCodigo("JPY");
//        tc.setMoneda("Yen Japonés");
//        tc.setTipoValor(0.033);
//        tipoCambioService.guardar(tc);
//        
//        tc=new TipoCambio();
//        tc.setCodigo("MXN");
//        tc.setMoneda("Peso Mexicano");
//        tc.setTipoValor(0.180);
//        tipoCambioService.guardar(tc);
//        
//        tc=new TipoCambio();
//        tc.setCodigo("PEN");
//        tc.setMoneda("Soles");
//        tc.setTipoValor(1.0);
//        tipoCambioService.guardar(tc);
//
//        //admin
//        //$2a$10$j7R79ziuc4tFhBILcVY.B.XF3tKFweQV208N8XKv5zKeKuhDDeIYy
//        Usuario usuario=new Usuario("admin","admin","jimy.ccatamayo@gmail.com","$2a$10$j7R79ziuc4tFhBILcVY.B.XF3tKFweQV208N8XKv5zKeKuhDDeIYy");
//        Set<Rol> roles = new HashSet<>();
//        roles.add(rolAdmin);
//        usuario.setRoles(roles);
//        usuarioService.save(usuario);




/*
insert into tipo_cambio (id,descripcion,codigo,tc_soles)values(1,'Dólar estadounidense','USD', 3.729);
insert into tipo_cambio (id,descripcion,codigo,tc_soles)values(2,'Euro','EUR', 4.353);
insert into tipo_cambio (id,descripcion,codigo,tc_soles)values(3,'Yen Japonés','JPY', 0.033);
insert into tipo_cambio (id,descripcion,codigo,tc_soles)values(4,'Soles','PEN', 1);
insert into tipo_cambio (id,descripcion,codigo,tc_soles)values(5,'Peso Mexicano','MXN', 0.180);
*/
    }
}




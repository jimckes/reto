package com.bcp.reto.response;

import java.io.Serializable;

public class TipoCambioResponse implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Double monto;
    private Double montoTipoCambio;
    private String monedaOrigen;
    private String monedaDestino;
    private String tipoCambio;
    public Double getMonto() {
        return monto;
    }
    public void setMonto(Double monto) {
        this.monto = monto;
    }
    public Double getMontoTipoCambio() {
        return montoTipoCambio;
    }
    public void setMontoTipoCambio(Double montoTipoCambio) {
        this.montoTipoCambio = montoTipoCambio;
    }
    public String getMonedaOrigen() {
        return monedaOrigen;
    }
    public void setMonedaOrigen(String monedaOrigen) {
        this.monedaOrigen = monedaOrigen;
    }
    public String getMonedaDestino() {
        return monedaDestino;
    }
    public void setMonedaDestino(String monedaDestino) {
        this.monedaDestino = monedaDestino;
    }
    public String getTipoCambio() {
        return tipoCambio;
    }
    public void setTipoCambio(String tipoCambio) {
        this.tipoCambio = tipoCambio;
    }



}